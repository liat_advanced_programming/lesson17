#include "Client.h"
#include <cstdlib>
#include <thread>

CryptoDevice cryptoDevice;
using std::string;
using std::cout;
using std::cin;
using std::endl;

#define USERNAME1 "Lithium"
#define PASS1 "6.94"
//Decrypted password: 6.94

#define USERNAME2 "Calcium"
#define PASS2 "40.078"
//Decrypted password: 40.078

#define USERNAME3 "Gold"
#define PASS3 "196.97"
//Decrypted password: 196.97

int process_client(client_type &new_client)
{
	while (1)
	{
		std::memset(new_client.received_message, 0, DEFAULT_BUFLEN);

		if (new_client.socket != 0)
		{


			int iResult = recv(new_client.socket, new_client.received_message, DEFAULT_BUFLEN, 0);

            std::string strMessage(new_client.received_message);

			// some logic, we dont want to decrypt notifications sent by the operator
			// our protocol says - ": " means notification from the operator
			size_t position = strMessage.find(": ") + 2;
            std::string prefix = strMessage.substr(0, position);
            std::string postfix = strMessage.substr(position);
            std::string decrypted_message;

			//this is the only notification we use right now :(
            if (postfix != "Disconnected") {
                //please decrypt this part!
                decrypted_message = cryptoDevice.decryptAES(postfix);
            }
            else 
            {
                //dont decrypt this - not classified and has not been ecrypted! 
                //trying to do so may cause errors
                decrypted_message = postfix;
            }

            if (iResult != SOCKET_ERROR) 
            {
                std::cout << prefix + decrypted_message << std::endl;
            }            
			else
			{
                std::cout << "recv() failed: " << ::WSAGetLastError() << std::endl;
				break;
			}
		}
	}

	if (::WSAGetLastError() == WSAECONNRESET)
        std::cout << "The server has disconnected" << std::endl;

	return 0;
}

bool Register()
{
	string usernames[3] = { USERNAME1, USERNAME2, USERNAME3 };
	string passwords[3] = { PASS1, PASS2, PASS3 };
	string username;
	string password;

	CryptoDevice a;
	cout << " - Register Secretly -" << endl << endl;
	cout << "Enter Username: ";
	cin >> username;

	//Hide user original input
	HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
	DWORD mode = 0;
	GetConsoleMode(hStdin, &mode);
	SetConsoleMode(hStdin, mode & (~ENABLE_ECHO_INPUT));

	cout << "Enter Password: ";
	cin >> password;
	cout << a.Md5(password) << endl;

	//Clean up input hiding
	SetConsoleMode(hStdin, mode);

	bool userflag = false;
	bool passflag = false;

	for (int i = 0; i < 3; i++)
	{
		if (username == usernames[i])
		{
			userflag = 1;
		}
		if (password == passwords[i])
		{
			passflag = 1;
		}
	}

	return (passflag && userflag);
}

int main()
{
	WSAData wsa_data;
	struct addrinfo *result = NULL, *ptr = NULL, hints;
    std::string sent_message = "";
	client_type client = { INVALID_SOCKET, -1, "" };
	int iResult = 0;
    std::string message;

    std::cout << "Starting Client...\n";

	// Initialize Winsock
	iResult = ::WSAStartup(MAKEWORD(2, 2), &wsa_data);
	if (iResult != 0) {
        std::cout << "WSAStartup() failed with error: " << iResult << std::endl;
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

    std::cout << "Connecting...\n";

	// Resolve the server address and port
	iResult = getaddrinfo(IP_ADDRESS, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
        std::cout << "getaddrinfo() failed with error: " << iResult << std::endl;
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		client.socket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (client.socket == INVALID_SOCKET) {
            std::cout << "socket() failed with error: " << ::WSAGetLastError() << std::endl;
			::WSACleanup();
			std::system("pause");
			return 1;
		}

		// Connect to server.
		iResult = ::connect(client.socket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(client.socket);
			client.socket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (client.socket == INVALID_SOCKET) {
        std::cout << "Unable to connect to server!" << std::endl;
		::WSACleanup();
		std::system("pause");
		return 1;
	}

    std::cout << "Successfully Connected" << std::endl;

	//Register Secretly
	int count = 0;
	for (int i = 0; i < 3; i++)
	{
		if (Register() == false)
		{
			count++;
			cout << "Error: invalid username or password" << endl;
			cout << 3 - count << " more attempts were left" << endl << endl;
		}
		else
		{
			system("cls");
			cout << "Welcome ! write your messages" << endl;
			break;
		}
	}
	if (count == 3)
	{
		system("cls");
		cout << "Registration failed" << endl;
		system("PAUSE");
		return 1;
	}
	

	//Obtain id from server for this client;
	::recv(client.socket, client.received_message, DEFAULT_BUFLEN, 0);
	message = client.received_message;

	if (message != "Server is full")
	{
		client.id = std::atoi(client.received_message);

        std::thread my_thread(process_client, std::ref(client));

		while (1)
		{
            std::getline(std::cin, sent_message);

			//top secret! please encrypt
			std::string cipher = cryptoDevice.encryptAES(sent_message);

			iResult = ::send(client.socket, cipher.c_str(), cipher.length(), 0);

			if (iResult <= 0)
			{
                std::cout << "send() failed: " << ::WSAGetLastError() << std::endl;
				break;
			}


		}

		//Shutdown the connection since no more data will be sent
		my_thread.detach();
	}
	else
        std::cout << client.received_message << std::endl;

    std::cout << "Shutting down socket..." << std::endl;
	iResult = ::shutdown(client.socket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
        std::cout << "shutdown() failed with error: " << ::WSAGetLastError() << std::endl;
		::closesocket(client.socket);
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	::closesocket(client.socket);
	::WSACleanup();
	std::system("pause");
	return 0;
}

